package com.example.server;

import com.example.server.plugin.OSGiProvider;
import com.example.server.xmpp.core.XMPPServer;
import com.example.server.xmpp.database.IDatabaseProvider;
import com.example.server.xmpp.database.SQLDatabase;

import java.math.BigInteger;
import java.security.SecureRandom;

import javax.xml.stream.XMLStreamException;

public class Server {
    private static SecureRandom random = new SecureRandom();

    public static void main(String[] args) {

        IDatabaseProvider provider;

        String keyStoreFile = "server.ks";
        String keyStorePassword = "keystore";
        provider = new SQLDatabase("postgresql", "localhost", "5432", "postgres", "postgres", "1234");
        try {
            new OSGiProvider();
            new XMPPServer(5222, keyStoreFile, keyStorePassword, provider);
        } catch (NumberFormatException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public static String getRandomString(int length) {
        return new BigInteger(130, random).toString(length);
    }
}
